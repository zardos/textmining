import cPickle
import numpy as np
import theano
import theano.tensor as T
from RAE import RAE

def getParameters(parameters,cost):
    params=[]
    for param in parameters:
        getParam = T.grad(cost,param)
        params.append(getParam)
    return params


def getUpdates(params, qparams,lossRate):
    updates = []
    for param, gparam in zip(params, gparams):
        update = lossRate * gparam
        updates.append((param, param - update))
    return updates  


if __name__ == '__main__':
    
    #################
    ## Initialization
    #################
    rng = np.random.RandomState(1234)
    dimension = 8
    data = np.eye(dimension)

    X = T.dmatrix(name="X")
    rae = RAE(X=X, rng=rng, dimension=dimension, data=data)

    lossRate = 0.2
    cost = rae.loss
    loss = rae.loss
    params = rae.params 

    gparams = getParameters(params, cost)
    updates = getUpdates(params, gparams, lossRate) 
    
    train_error = theano.function(inputs=[], outputs=rae.loss, givens={X: data})
    train =theano.function(inputs=[],outputs=cost,updates=updates,givens={X: data})
    

    ####################
    ## Calculation #####
    ####################
    for epoch in xrange(50000):
        loss = train()
        print "%d\t%f" % (epoch + 1, loss)

    init_W = rae.W.get_value()
    print 'Initial weights:\n\n',init_W
    print '\n\nrae.W.get_value()\n\n', rae.W.get_value()

    print '\n\nDATA=\n\n',
    for i in data:
        print i,'\n'
    print '\n'


    for i in xrange(len(data)-1):
        print 'i =', i
        if i == 0:
            print "\nencode step 0"
            encode = rae.encode(T.concatenate([data[i], data[i+1]], axis=0))
            print encode.eval()

            print "\ndecode step 0"
            decode = rae.decode(encode)
            print decode.eval()
            continue

        print "\nencode step " ,i
        encode = rae.encode(T.concatenate([encode, data[i+1]], axis=0))
        print encode.eval()

        print "\ndecode step ", i
        decode = rae.decode(encode)
        print decode.eval()


    ######################
    ### Saving Trained RAE
    ######################
    save = file('./model.pkl','wb')
    cPickle.dump(rae, save, protocol=cPickle.HIGHEST_PROTOCOL)
    save.close()

    #######################
    ### Loading Trained RAE
    #######################
    load = file('./model.pkl','rb')
    model = cPickle.load(load)
    load.close()

    ########################
    ### Checking Trained RAE
    ########################
    for i in xrange(len(data)-1):
        print 'i =', i
        if i == 0:
            print "\nencode CheckingStep 0"
            encode = model.encode(T.concatenate([data[i], data[i+1]], axis=0))
            print encode.eval()

            print "\ndecode CheckingStep 0"
            decode = model.decode(encode)
            print decode.eval()
            continue

        print "\nencode CheckingStep " ,i
        encode = model.encode(T.concatenate([encode, data[i+1]], axis=0))
        print encode.eval()

        print "\ndecode CheckingStep ", i
        decode = model.decode(encode)
        print decode.eval()