#!/usr/bin/env python

import json
import simplejson
import os

def createDataset(path):
    dataset = []
    for directory in ["vulnerable", "notVulnerable"]:
        directoryPath = os.path.join(path, directory)
	print directoryPath
        if directory == "vulnerable":
            dataset + addFilesToDataset(directoryPath, -1)
	    print dataset
        else:
            dataset + addFilesToDataset(directoryPath, 1)
    return simplejson.dumps(dataset, indent=4, sort_keys=True)

def addFilesToDataset(path, vulnerable):
    files = os.listdir(path)
    return [ {"code": open(os.path.join(path, name), "r").read().encode("base64"),
              "label": vulnerable} for name in files ]

bla = createDataset('/home/muchacho/uni/semester11/textmining/sources/Trainingdata/TrainingDatasetFiles/sqli/Training/')