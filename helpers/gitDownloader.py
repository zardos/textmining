from BeautifulSoup import BeautifulSoup
import urllib2
import re
import pickle
import argparse
import git
import subprocess
import os

DOWNLOAD_PATH = '/home/cbo/textmining/RAE/Trainingdata/trainingdata/'

def get_args():
    parser = argparse.ArgumentParser(description='Use --load-pickle <file>, --save-pickle <file>, --url <url>, --dpath')
    parser.add_argument("--load",type=str, required=False)
    parser.add_argument("--save",type=str, required=False)
    parser.add_argument("--url",type=str, required=False)
    parser.add_argument("--dpath",type=str, required=False)
    
    args = parser.parse_args()
    if args.load is not None:
    load = args.load
    else: load = None

    if args.save is not None:
        save = args.save
    else: save = None
    
    if args.url is not None:
        url = args.url
    else: url = None

    if args.url is not None:
        dpath = args.dpath
    else: dpath = DOWNLOAD_PATH

    return load, save, url, dpath


def get_links(url, save, load):
    links = []

    if url:
        html_page = urllib2.urlopen(url)
        soup = BeautifulSoup(html_page)

        for link in soup.findAll('a'):
            #if not str(link.get('href')).endswith('.pip'):
            #    continue

            if url.startswith('https://gitorious.org/'):
                links.append(('https://gitorious.org'+link.get('href'))[:-1])        

            else:
                links.append(link.get('href'))

        if save:
            pickle.dump(links,open(save,'wb+'))
            print '[*] pickle-file '+save+' created'

    if load:
        links = pickle.load(open(load,'rb'))
        print '[*] pickle-file '+load+' loaded'
    
    return links


def git_clone(link, dpath):
    subprocess.call('export GIT_SSL_NO_VERIFY=1', shell=True)
    print '[+] cloning git-repo ', link
    
    try:
        git.Git().clone(str(link))#, dpath)
    except Exception:
        print '[-] couldn\'t clone ', link
        pass


def main():
    load, save, url, dpath = get_args()
    links = get_links(url, save, load)
    print '[*] ',len(links),' links successfully loaded'

    dir = os.getcwd()
    os.chdir(str(dpath))
    for link in links:
        git_clone(link, dpath)
    os.chdir(dir)


    print '[*] git cloning done, check directory '+dpath
    print 'now run createASTDataSet.py'


if __name__ == "__main__":
    main()