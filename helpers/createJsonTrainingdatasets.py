import sys
import glob
import errno
import pprint
import simplejson
import re

path = '/home/muchacho/workspace/textmining/final/Trainingdata/sqli/training/injectable/*.php'   
files = glob.glob(path)   
dataset = {"datasets":[]}

for name in files:
    try:
        with open(name) as f: # No need to specify 'r': this is the default.
            payload = f.read()
            payload = payload.replace('\n','#')
            payload = payload.replace('\t',' ')
            payload = payload.replace('\r','')
            print len(payload)

            payload = re.sub(' +',' ', payload)

            dataset.setdefault('datasets',[0]).append({'code':payload.encode('base64'),'label':'injectable'})
            f.close()
            
    except IOError as exc:
        if exc.errno != errno.EISDIR: # Do not fail if a directory is found, just ignore it.
            raise # Propagate other kinds of IOError.

#pprint.pprint(dataset)
#print len(dataset('datasets')[0])
json_data = simplejson.dumps(dataset, indent=4, sort_keys=True)
#print json_data
with open('/home/muchacho/workspace/textmining/final/trainingData-sqli.json','w') as outfile:
	outfile.write(json_data)