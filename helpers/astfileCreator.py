#! /usr/bin/python3
from __future__ import print_function
import sys
import os
import subprocess
import argparse


#AST_PATH = '/home/cbo/textmining/RAE/Trainingdata/AST/'
#HELPER_PATH = '/home/cbo/textmining/RAE/helpers/'

def get_args():
    parser = argparse.ArgumentParser(description='Creates ASTfiles from c sources')
    parser.add_argument("--cpath",type=str, required=True, help='Location of C-sources')
    parser.add_argument("--dpath",type=str, required=True, help='AST-Destination location')
    
    args = parser.parse_args()
    cpath = args.cpath
    dpath = args.dpath

    return cpath, dpath



def createRAETrainingSet(cpath, dpath):
    fileExtensions = [".c",".h"]
    dataset = []
    mylocation = os.getcwd()
    for root, dirs, files in os.walk(cpath):
        for fileExtension in fileExtensions:
            for name in [n for n in files if n.endswith(fileExtension)]:
                filepath = os.path.join(root, name)
        
                os.chdir(root+'/')

                subprocess.call(["gcc","-fdump-tree-original","-w", name])
                subprocess.call(['mv',name+'.003t.original', dpath])
                os.chdir(mylocation)    
    
    print('[*] AST-creation done')

    subprocess.call("find "+dpath+"* -type f -exec grep -q 'error' {} \; -delete", shell=True)
    subprocess.call("find "+ dpath+"* -size  0 -print0 |xargs -0 rm", shell=True)
    print('[*] cleaning done')



def normalize(cCodeUgly):
    cCodeUgly = cCodeUgly.decode("utf-8")
    stringList = cCodeUgly.split('\n')
    prefixes = ('.','#','..')
    suffixes = ('^','generated.')
    clearedPrefixes = []
    clearedPreSuffixes = []

    
    for prefix in prefixes:
        for line in range(len(stringList)-1):
            if stringList[line].startswith(prefix):
                continue
            else: clearedPrefixes.append(stringList[line])

    for suffix in suffixes:
        for line in range(len(clearedPrefixes)-1):
            if clearedPrefixes[line].endswith(suffix):
                continue
            else: clearedPreSuffixes.append(stringList[line])
    

    return '\n'.join(clearedPreSuffixes)
    

if __name__ == "__main__":
    cpath, dpath = get_args()

    #Using gcc for AST generation
    createRAETrainingSet(cpath,dpath)
    #dataset = normalize(cCodeUgly)