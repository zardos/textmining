import numpy as np
import theano
import theano.tensor as T
import cPickle

class RAE(object):
    def __init__(self, X, rng, dimension, data, W=None, b=None, U=None, c=None):
        
        #random state np.random.RandomState

        dv = dimension
        dh = dimension * 2

        self.dv = dv
        self.dh = dh

        self.initx = X[0]
        self.input = X[1:]

        if W == None:
            W = np.asarray(rng.uniform(
                low=-4 * np.sqrt(6. / (dv + dh)),
                high=4 * np.sqrt(6. / (dv + dh)),
                #theano.config.floatX sets to 32 or 64bit
                size=(dh, dv)), dtype=theano.config.floatX)

        if U == None:
            U = np.asarray(rng.uniform(
                low=-4 * np.sqrt(6. / (dv + dh)),
                high=4 * np.sqrt(6. / (dv + dh)),
                #theano.config.floatX sets to 32 or 64bit
                size=(dv, dh)), dtype=theano.config.floatX)

        if b == None:
            #theano.config.floatX sets to 32 or 64bit
            b = np.zeros((dv,), dtype=theano.config.floatX)

        if c == None:
            #theano.config.floatX sets to 32 or 64bit
            c = np.zeros((dh,), dtype=theano.config.floatX)

        self.W = theano.shared(value=W, name="W")
        self.U = theano.shared(value=U, name="U")
        self.b = theano.shared(value=b, name="b")
        self.c = theano.shared(value=c, name="c")

        self.params = [self.W, self.b, self.U, self.c]

        self.updates = []
        for param in self.params:
            init = np.zeros(param.get_value(borrow=True).shape, dtype=theano.config.floatX)
            self.updates.append((param, theano.shared(init)))

        def step(x1, x2):
            origin = T.concatenate([x2, x1], axis=0)
            encode = self.encode(origin)
            decode = self.decode(encode)
            return encode, decode, origin

        [encode, decode, origin], _ = theano.scan(
            fn=step,
            sequences=self.input,
            outputs_info=[self.initx, None, None]
        )

        self.L1 = abs(self.W.sum()) + abs(self.U.sum())
        self.L2_sqr = (self.W ** 2).sum() + (self.U ** 2).sum()

        self.loss = self.error(origin, decode)

    def encode(self, origin):
        return T.nnet.sigmoid(T.dot(origin, self.W) + self.b)

    def decode(self, encode):
        return T.dot(encode, self.U) + self.c

    def error(self, origin, decode):
        return T.mean((decode - origin) ** 2)

    
'''

def getParameters(parameters,cost):
    params=[]
    for param in parameters:
        getParam = T.grad(cost,param)
        params.append(getParam)
    return params


def getUpdates(params, qparams,lossRate):
    updates = []
    for param, gparam in zip(params, gparams):
        update = lossRate * gparam
        updates.append((param, param - update))
    return updates  



if __name__ == '__main__':
    
    #################
    ## Initialization
    #################
    rng = np.random.RandomState(1234)
    dimension = 8
    data = np.eye(dimension)

    X = T.dmatrix(name="X")
    rae = RAE(X=X, rng=rng, dimension=dimension, data=data)

    lossRate = 0.2
    cost = rae.loss
    loss = rae.loss
    params = rae.params 

    gparams = getParameters(params, cost)
    updates = getUpdates(params, gparams, lossRate) 
    
    train_error = theano.function(inputs=[], outputs=rae.loss, givens={X: data})
    train =theano.function(inputs=[],outputs=cost,updates=updates,givens={X: data})
    

    ####################
    ## Calculation #####
    ####################
    for epoch in xrange(50000):
        loss = train()
        print "%d\t%f" % (epoch + 1, loss)

    init_W = rae.W.get_value()
    print 'Initial weights:\n\n',init_W
    print '\n\nrae.W.get_value()\n\n', rae.W.get_value()

    print '\n\nDATA=\n\n',
    for i in data:
        print i,'\n'
    print '\n'


    for i in xrange(len(data)-1):
        print 'i =', i
        if i == 0:
            print "\nencode step 0"
            encode = rae.encode(T.concatenate([data[i], data[i+1]], axis=0))
            print encode.eval()

            print "\ndecode step 0"
            decode = rae.decode(encode)
            print decode.eval()
            continue

        print "\nencode step " ,i
        encode = rae.encode(T.concatenate([encode, data[i+1]], axis=0))
        print encode.eval()

        print "\ndecode step ", i
        decode = rae.decode(encode)
        print decode.eval()


    ######################
    ### Saving Trained RAE
    ######################
    save = file('./model.pkl','wb')
    cPickle.dump(rae, save, protocol=cPickle.HIGHEST_PROTOCOL)
    save.close()

    #######################
    ### Loading Trained RAE
    #######################
    load = file('./model.pkl','rb')
    model = cPickle.load(load)
    load.close()

    #########################
    ### Checking Trained RAE
    #########################
    for i in xrange(len(data)-1):
        print 'i =', i
        if i == 0:
            print "\nencode CheckingStep 0"
            encode = model.encode(T.concatenate([data[i], data[i+1]], axis=0))
            print encode.eval()

            print "\ndecode CheckingStep 0"
            decode = model.decode(encode)
            print decode.eval()
            continue

        print "\nencode CheckingStep " ,i
        encode = model.encode(T.concatenate([encode, data[i+1]], axis=0))
        print encode.eval()

        print "\ndecode CheckingStep ", i
        decode = model.decode(encode)
        print decode.eval()
'''