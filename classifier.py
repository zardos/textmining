#!/usr/bin/env python 
from pybrain.tools.shortcuts import buildNetwork
from pybrain.tools.functions import sigmoid
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules.neuronlayer import NeuronLayer
from pybrain.structure import RecurrentNetwork
from pybrain.structure import FullConnection,LinearLayer,SigmoidLayer

#For saving neural network
from pybrain.tools.shortcuts import buildNetwork
from pybrain.tools.xml.networkwriter import NetworkWriter
from pybrain.tools.xml.networkreader import NetworkReader

import base64
import math
import json
from pprint import pprint
import re
import hashlib

from pycparser import c_parser
import StringIO

 
charStr = "abcdefghijklmnopqrstuvwxyz _-!@#$%^&*()[]{}+=<>,./?;:'1234567890\|`~"+'"'

######################################################
##### recurrent neural network and Trainer setup
######################################################
INPUT_LAYER = 1000#2250
HIDDEN_LAYER = 500
OUTPUT_LAYER = 1
LAYER_COUNT = 10
EPOCHS = 100
EPOCHITERATIONS = 10

###Supervised Trainer Settings
INPUT_LAYER_AXIS = INPUT_LAYER

###Trainer setup
LEARNINGRATE = 0.001
MOMENTUM=0.0
WEIGHTDECAY=0.0


######################################################
############## C code to AST
######################################################
def CtoASTconverter(cCode):
    tree = StringIO.StringIO()
    parser = c_parser.CParser()
    AST = parser.parse(cCode)
    AST.show(tree)
    return tree.getvalue()


######################################################
#### Save and restore trained recurrent neural network
######################################################
def convertData(character):
    #Return index of character in charStr
    converted = (charStr.find(character),)
    #print character, ' converted: ',converted
    return converted


######################################################
# Normalization and Base64decoding of TrainingData
######################################################
def normalizeString(textToNormalize):
    decodeBase64 = textToNormalize.decode("base64")
    tabsReplace = decodeBase64.replace('\t',' ')
    newLinesReplaced = tabsReplace.replace('\n', '#')
    newLinesReplaced = newLinesReplaced.replace('\r','')
    removedMoreThanTwoSpaces = re.sub(' +',' ', newLinesReplaced)
    lowerCased = removedMoreThanTwoSpaces.lower()
    return lowerCased


######################################################
##### loading Trainingdatasets from JSON-File
######################################################
def loadTrainingData():
    #loading trainingdata from JSON-File
    #with open("./Trainingdata/textmining/train.json") as trainingFile:
    with open("./Trainingdata/trainingData-sqli.json") as trainingFile:
        rawTrainingData = json.load(trainingFile)

    trainingSet1 = []
    #trainingSet2 = []
    #trainingSet3 = []
    
    #converting JSON to tuple
    for i in rawTrainingData["datasets"]:
       
        #Check lable, needed for supervising Training (why?)
        if i['label'] is 'injectable': label=-1
        else: label=1

        #Normalization and decoding of rawTrainingdata to base64-code
        #decodedTupple = (normalizeString(i['code']), label)
        decodedTupple = ((i['code']).decode('base64'), label)
        
        trainingSet1.append(decodedTupple)
        #trainingSet2.append((i['code'].decode,i['label']))
        #trainingSet3.append((i['code'],i['label']))

    return trainingSet1 #, trainingSet2, trainingSet3



#######################################################
### Create Supervised Dataset
#######################################################
def superVisedTraining(data):
    #Creating SupervisedDataSets from TrainingData

    dataSet = SupervisedDataSet(INPUT_LAYER_AXIS, 1)
    
    for x in data:
        
        #print(x[0])

        if len(x[0]) > INPUT_LAYER_AXIS:
            continue

        example = tuple(x[0].lower())
        examplePrime = ()
        
        for character in example:
            examplePrime += convertData(character)
            #print examplePrime

        example = examplePrime
        for i in range((INPUT_LAYER_AXIS - len(example))):
            example += (0,)
       
        for i in range(4):
            dataSet.addSample(example, (x[1],))

        #print "run ", i," of ", len(data) ,' of SuperVisedTraining, be patient...', dataSet

    return dataSet

######################################################
##### creating recurrent neural network
######################################################
def createRecurrentNeuralNetwork(inputlayer=INPUT_LAYER, hidden=HIDDEN_LAYER, output=OUTPUT_LAYER):

    layerCount = LAYER_COUNT
    #print '############################################# inputlayer', inputlayer

    rnn = RecurrentNetwork()

    #Initialize Input and hidden layer
    rnn.addInputModule(LinearLayer(inputlayer, name='in'))
    for x in range(layerCount):
        rnn.addModule(SigmoidLayer(hidden, name='hidden'+str(x)))

    #Initialize Output-Layer
    rnn.addOutputModule(LinearLayer(output, name='out'))

    #Initialize connections between layers
    rnn.addConnection(FullConnection(rnn['in'], rnn['hidden1'], name='cIn'))
    for x in range(layerCount-1):
        rnn.addConnection(FullConnection(rnn[('hidden'+str(x))], rnn['hidden'+str(x+1)], name=('c'+str(x+1))))

    rnn.addConnection(FullConnection(rnn['hidden'+str(layerCount-1)], rnn['out'], name='cOut'))

    #Prepare the network for activation by sorting the internal datastructure.
    #Needs to be called before activation.
    rnn.sortModules()
    return rnn
    

######################################################
#### Train Recurrent Neural Network
######################################################
def trainRecurrentNeuralNetwork(rnn, superDataSet): 
    # trainerinitialization and backpropagation settions
    trainer = BackpropTrainer(rnn, superDataSet, learningrate=LEARNINGRATE, momentum=MOMENTUM, weightdecay=WEIGHTDECAY, verbose=True)

    #run 10 iterations of each EPOCHS
    for x in range(EPOCHITERATIONS):
        trainer.trainEpochs(epochs=EPOCHS)
        print(EPOCHS," 100 more epochs")

    #Test
    print 'MD5 of trained rnn: ', hashlib.md5(str(rnn)).hexdigest()

    #Saving recurrent neural network in file
    NetworkWriter.writeToFile(rnn, 'trained-net.xml')
    #for loading: net = NetworkReader.readFrom('trained-net.xml')
    return rnn


######################################################
#### Save and restore trained recurrent neural network
######################################################
#save trained network to file
def safeTrainedNetwork(rnn):
# Using NetworkWriter its possible to retrain
# the network again.
    NetworkWriter.writeToFile(rnn, 'trained-net-sqli.xml')

#restore trained network from file
def loadlNeuralNetwork():
    rnn = NetworkReader.readFrom('trained-net-sqli.xml')
    return rnn


######################################################
#### Get User Data, Converting and Padding
######################################################
def getInputData():
    dataToPredict = list(raw_input("input: "))
    dataToPredictPrime = []
    
    #convert characters to index
    for character in dataToPredict:
        dataToPredictPrime.append(convertData(character))

    dataToPredict = dataToPredictPrime

    #Do Padding: If input is shorter than INPUT_LAYER_AXIS pad with ZEROS
    for x in range((INPUT_LAYER_AXIS - len(dataToPredict))):
        dataToPredict.append(0)

    return dataToPredict




######################################################
#### do all the stuff
######################################################
def main():
    ### CREATING RECURRENT NEURAL NETWORK
    # createRecurrentNeuralNetwork() <- takes 3 params: inputlayer=INPUT_LAYER, hidden=HIDDEN_LAYER, output=OUTPUT_LAYER
    myRecurrentNeuralNetwork = createRecurrentNeuralNetwork()
    import hashlib
    print 'MD5 of initialized myRecurrentNeuralNetwork: ', hashlib.md5(str(myRecurrentNeuralNetwork)).hexdigest()
    

    #### LOADING AND CONVERTING TRAININGDATA FROM FILE
    #trainingData, trainingData2, trainingData3 = loadTrainingData()
    trainingData = loadTrainingData()
    #print trainingData

    
    #### EXECUTE SUPERVISED TRAINING
    superDataSet = superVisedTraining(trainingData)
 

    ### TRAIN RECURRENT NEURAL NETWORK
    # Also initialization of trainer
    myTrainedRecurrentNeuralNetwork = trainRecurrentNeuralNetwork(myRecurrentNeuralNetwork, superDataSet)
    print 'MD5 of trained myTrainedRecurrentNeuralNetwork: ',hashlib.md5(str((myTrainedRecurrentNeuralNetwork))).hexdigest()

    
    ### READ TESTDATA  
    ### CONVERT TESTDATA TO CHARSETINDEX-INT
    ### DO PADDING
    dataToPredict = getInputData()

    ### MAKE PREDICTION
    prediction = myTrainedRecurrentNeuralNetwork.activate(dataToPredict)
    print("prediction: ",prediction)

    '''
    CREATE THREADING
    '''
    #make predictions with calculated rnn:
    while True:
        toPredict = getInputData()       
        prediction = myTrainedRecurrentNeuralNetwork.activate(toPredict)
        print("prediction: ",answer)


if __name__ == "__main__":
    main()