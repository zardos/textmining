// 1. https://wordpress.org/support/topic/sql-injection-vulnerability-possible-patch 

$args = array(
	$stuff,
	$now,
	$current_date,
	'publish',
	$post->ID,
);
$sql = "
	SELECT DISTINCT ID
	FROM ".$wpdb->posts."
	WHERE MATCH (post_title,post_content) AGAINST (%s)
	AND post_date < %s
	AND post_date >= %s
	AND post_status = %s
	AND ID != %d
";
// I really hope the below is already sanitized! If not do something like
// implode(',', array_map('intval', array_map('trim', explode(",", $crp_settings['exclude_post_ids']))))
// but over multiple lines to avoid PHP reference warnings
if ($crp_settings['exclude_post_ids']!='')
	$sql .= "AND ID NOT IN (".$crp_settings['exclude_post_ids'].") ";
$sql .= " AND (";
foreach ($post_types as $post_type) {
	if ( $multiple ) $sql .= ' OR ';
	$sql .= " post_type = '%s";
	$multiple = true;

	$args[] = $post_type;
}
$args[] = $limit;
$sq .= " ) LIMIT %d";

$results = $wpdb->query($wpdb->prepare($sql, $args));
