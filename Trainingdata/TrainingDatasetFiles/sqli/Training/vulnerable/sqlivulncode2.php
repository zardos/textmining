CREATE TABLE `users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `creditcard` varchar(200) NOT NULL,
  PRIMARY KEY  (`id`)
);


INSERT INTO `users` (`id`, `username`, `password`, `creditcard`) VALUES 
(1, 'Sidewinder', 'monkey', '0123456789987654'),
(2, 'John', 'password!', '3123456769384659'),
(3, 'Bob', '1234', '7133116752374638');



#vulnerable php code


<?php 
if(empty($_GET['username'])) {

echo "<form method='GET' action='login.php'>"
    ."Username: <input type='text' name='username' /><br />"
    ."Password: <input type='text' name='password' /><br />"
    ."<input type='submit' value='Login' />"
    ."</form>";
}

if(get_magic_quotes_gpc()) {
   $username = stripslashes($_GET['username']);
   $password = stripslashes($_GET['password']);
}

$link = mysql_connect($dbhost, $dbuser, $dbpass) 
    or die('Could not connect: ' . mysql_error());

mysql_select_db($mysqldb, $link) 
    or die('Could not select database.');

$query = mysql_query("SELECT * FROM `users` "
        . "WHERE `username` = '$username' "
        . "AND `password` = '$password'")
    or die('Could not select database.');

$row = mysql_fetch_assoc($query);

if(mysql_num_rows($query) >= 1) {
    echo "Hello {$row['username']}!<br />";
    echo "Your credit card number is: {$row['creditcard']}";
}
?>

#http://forum.codecall.net/topic/38299-php-sql-injections/
