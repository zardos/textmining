Integer overflow

#include <stdio.h>
#include <string.h>

void main(int argc, char *argv[]) {
    int i = atoi(argv[1]);         // input from user
    unsigned short s = i;          // truncate to a short
    char buf[50];                  // large buffer

    if (s > 10) {                  // check we're not greater than 10
        return;
    }

    memcpy(buf, argv[2], i);       // copy i bytes to the buffer
    buf[i] = '\0';                 // add a null byte to the buffer
    printf("%s\n", buf);           // output the buffer contents

    return;
}