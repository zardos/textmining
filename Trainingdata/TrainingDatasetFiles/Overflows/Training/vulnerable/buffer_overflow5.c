/* stack.c */ 
/* This program has a buffer overflow vulnerability. */ 
/* Our task is to exploit this vulnerability */
 #include<stdlib.h>
 #include<stdio.h>
 #include<string.h>
int func (char *str) 
{ 
    char buffer[12]; 
    /* The following statement has a buffer overflow problem */ 
    strcpy(buffer, str);
    return 1;
 } 

 int main(int argc, char **argv) 
{ 
    char str[517]; 
    FILE *badfile; 
    badfile = fopen("badfile", "r");
    fread(str, sizeof(char), 517, badfile);
    func (str); 
    printf("Returned Properly\n"); 
    return 1; 
}

#include int main( ) 
{ 
    char *name[2];
    name[0] = ''/bin/sh''; 
    name[1] = NULL; 
    execve(name[0], name, NULL); 
}