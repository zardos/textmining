#include <stdio.h>

int main(int argc, char * argv[]) {

    char buf[256];

    if(argc == 1) {

        printf("Usage: %s input\n", argv[0]);
        exit(0);

    }

    strcpy(buf,argv[1]);
    printf("%s", buf);

}