/* This program has a buffer overflow vulnerability. */ 
/* However, it is protected by StackGuard */
  #include<stdlib.h>
 #include<stdio.h>
 #include<string.h>

int func (char *str)
 { 
int canaryWord = secret;
 char buffer[12]; 
/* The following statement has a buffer overflow problem */ 
strcpy(buffer, str);
 if (canaryWord == secret)
 // Return address is not modified return 1; 
else 
// Return address is potentially modified { ... error handling ... }
 } 
static int secret; 
// a global variable int main(int argc, char **argv)
 { 
// getRandomNumber will return a random number secret = getRandomNumber(); char str[517]; 
FILE *badfile; 
badfile = fopen("badfile", "r"); 
fread(str, sizeof(char), 517, badfile); 
func (str); 
printf("Returned Properly\n");
 return 1; 
}

void bad_function(char *input)
{
char dest_buffer[32];
 
strcpy(dest_buffer, input);
printf("The first command-line argument is %s.\n", dest_buffer);
}
 
int main(int argc, char *argv[])
{
 
if (argc > 1)
{
bad_function(argv[1]);  
}
else
{
printf("No command-line argument was given.\n");
}
 
return 0;
}

int main(int argc, char *argv[])
{
char *dest_buffer;
 
dest_buffer = (char *) malloc(32);
 
if (NULL == dest_buffer)
return -1;
 
if (argc > 1)
{
strcpy(dest_buffer, argv[1]);
printf("The first command-line argument is %s.\n", dest_buffer);
}
else
{
printf("No command-line argument was given.\n");
}
 
free(dest_buffer);
 
return 0;
}