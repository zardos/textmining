import os
from flask import (Flask, abort, flash, Markup, redirect, render_template,
                   request, Response, session, url_for)

import functools


ADMIN_PASSWORD = 'secret'
APP_DIR = os.path.dirname(os.path.realpath(__file__))
DEBUG = False
SECRET_KEY = 'shhh, this is our super duper hyper mega secret key!'  # Used by Flask to encrypt session cookie.
SITE_WIDTH = 800

app = Flask(__name__)
app.config.from_object(__name__)



#######################
## Login/Logout #######
#######################
def login_required(fn):
    @functools.wraps(fn)
    def inner(*args, **kwargs):
        if session.get('logged_in'):
            return fn(*args, **kwargs)
        return redirect(url_for('login', next=request.path))
    return inner


@app.route('/login/', methods=['GET', 'POST'])
def login():
    next_url = request.args.get('next') or request.form.get('next')
    if request.method == 'POST' and request.form.get('password'):
        password = request.form.get('password')
        if password == app.config['ADMIN_PASSWORD']:
            session['logged_in'] = True
            session.permanent = True  # Use cookie to store session.
            flash('You are now logged in.', 'success')
            return redirect(next_url or url_for('index'))
        else:
            flash('Incorrect password.', 'danger')
    return render_template('login.html', next_url=next_url)


@app.route('/logout/', methods=['GET', 'POST'])
def logout():
    if request.method == 'POST':
        session.clear()
        return redirect(url_for('index'))
    return render_template('logout.html')




#######################
## Views ##############
#######################
@app.route('/', methods=['GET','POST'])
def index():
	if request.method == 'POST':
		if request.form.get('usersnippet'):
			flash('Snippet not injectable!','success')
		else:
			flash('snippet injectable','danger')
	return render_template('index.html')


@app.route('/dashboard/', methods=['GET'])
@login_required
def dashboard():
        #flash(request.form.get('snippet') + ' Entry created successfully.', 'success')
        #flash('Snippet is required.', 'danger')
    return render_template('dashboard.html')


@app.route('/newrnn/', methods=['GET','POST'])
@login_required
def newrnn():
        #flash(request.form.get('snippet') + ' Entry created successfully.', 'success')
        #flash('Snippet is required.', 'danger')
    return render_template('newrnn.html')


@app.route('/addtrainingdata/', methods=['GET', 'POST'])
@login_required
def addtrainingdata():
    if request.method == 'POST':
        if request.form.get('snippet'):
            flash(request.form.get('snippet') + ' Entry created successfully.', 'success')
        else:
            flash('Snippet is required.', 'danger')
    return render_template('addtrainingdata.html')




#######################
## APP Initialization #
#######################
@app.template_filter('clean_querystring')
def clean_querystring(request_args, *keys_to_remove, **new_values):
    querystring = dict((key, value) for key, value in request_args.items())
    for key in keys_to_remove:
        querystring.pop(key, None)
    querystring.update(new_values)
    return urllib.urlencode(querystring)


@app.errorhandler(404)
def not_found(exc):
    return Response('<h3>404 Not found</h3>'), 404


def main():
    app.run(debug=True)


if __name__ == '__main__':
    main()